import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import {getCurrentUser}  from '../../services/authentication';

// @ts-ignore
export const PrivateRoute = ({ component: Component, ...rest }) => {
    return (
        <Route {...rest} render={props => {
            if (!getCurrentUser()) {
                return <Redirect to={{pathname: '/login', state: {from: props.location}}}/>
            }
            return <Component {...props} />
        }}/>
    );
}