import {InputLabel} from "@material-ui/core";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import React, {useEffect, useState} from "react";


export default ({label, value, handleChange, options}: Props) => {

    const inputLabel = React.useRef<HTMLLabelElement>(null);
    const [labelWidth, setLabelWidth] = useState(0);

    useEffect(() => {
        setLabelWidth(inputLabel.current!.offsetWidth);
    }, []);

    return (
        <FormControl variant="outlined" fullWidth>
            <InputLabel ref={inputLabel}  id="simple-select-outlined-label">
                {label}
            </InputLabel>
            <Select
                labelId="simple-select-outlined-label"
                id="simple-select-outlined"
                value={value}
                onChange={handleChange}
                labelWidth={labelWidth}
            >
                {
                    options.map((option) => {
                        return (<MenuItem value={option.value}>{option.label}</MenuItem>)
                    })
                }
            </Select>
        </FormControl>
    )
}

interface Props {
    label: string
    value: string
    handleChange: any
    options: {
        value: string | number
        label: string
    }[]
}