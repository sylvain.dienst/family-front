import TextField from "@material-ui/core/TextField";
import React, {useEffect, useState} from "react";

export default ({type, required, label, name, value, error, handleChange} : Props) => {

    return (
        <TextField
            type={type}
            variant="outlined"
            margin="normal"
            required={required}
            fullWidth
            id={name}
            label={label}
            name={name}
            autoComplete={type}
            autoFocus
            value={value}
            helperText={error}
            error={error.length > 0}
            onChange={handleChange}
        />
    )
}

interface Props {
    type: string
    label: string
    name: string
    value: string
    error: string
    handleChange: any
    required: boolean
}