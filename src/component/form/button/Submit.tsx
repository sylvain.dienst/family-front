import Button from "@material-ui/core/Button";
import Spinner from "react-bootstrap/Spinner";
import React, {useState} from "react";
import {makeStyles} from "@material-ui/core";
import {Simulate} from "react-dom/test-utils";
import Typography from "@material-ui/core/Typography";


const useStyles = makeStyles( theme => ({
        submit: {
            margin: theme.spacing(3, 0, 2),
        }
    })
)

export default ({handleSubmit, loading, buttonValue} : Props) => {

    const classes = useStyles()
    return (
        <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={handleSubmit}
            disabled={loading}
        >
            { loading
            ?
                <React.Fragment>
                    <Typography component="span">{buttonValue}...</Typography>
                </React.Fragment>
            :
                <React.Fragment>
                    <Typography component="span">{buttonValue}</Typography>
                </React.Fragment>
            }
        </Button>
    )
}

interface Props {
    handleSubmit: any
    loading: boolean
    buttonValue: string
}