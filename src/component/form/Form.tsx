import Container from "@material-ui/core/Container";
import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import {Typography} from "@material-ui/core";
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import Submit from "./button/Submit";

const useStyles = makeStyles(theme => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    }
}));

export default ({ children, titleForm, handleSubmit, loading, buttonValue}: Props ) => {

    const classes = useStyles();
    const theme = createMuiTheme()
    return (
        <div className={classes.paper}>
            <Typography variant="h5">{titleForm}</Typography>
            <form className={classes.form}>
                {children}
            <Submit handleSubmit={handleSubmit} loading={loading} buttonValue={buttonValue} />
            </form>
        </div>
    )
}

interface Props {
    children: JSX.Element | JSX.Element[],
    titleForm: JSX.Element | string
    handleSubmit: any
    loading: boolean
    buttonValue: string
}