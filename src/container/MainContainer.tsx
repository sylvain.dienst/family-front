import React from 'react';
import { createStyles, Theme, makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '../component/notUsed/AppBar/AppBar';
import {getCurrentUser} from "../services/authentication"
import Navbar from '../component/navbar/Navbar'
const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
        },
        content: {
            flexGrow: 1,
            padding: theme.spacing(3),
        },
        toolbar: theme.mixins.toolbar,
    }),
);

export default ({ children}: Props ) => {
    const classes = useStyles();
    let render
    if (!getCurrentUser()){
        render = (
            <div className={classes.root}>
                {children}
            </div>)
    } else {
        render = (
            <div className={classes.root}>
                <CssBaseline/>
                <Navbar />
                <main className={classes.content}>
                    <div className={classes.toolbar}/>
                    {children}
                </main>
            </div>
        )
    }
    return render
}

interface Props {
    children: JSX.Element | JSX.Element[]
}