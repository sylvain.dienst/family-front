import axios from 'axios'
import headerAuth from './header'

const BASE_URL= process.env.REACT_APP_BASE_API_URL;


const config = () =>{
  return {
    headers: { Authorization : headerAuth() }
  }
}

export const get = <T>(path: string = '') => {
  return axios.get<T>(`${BASE_URL}/${path}`, config()).then(res => res)
}

export const post = <T>(path: string, body: any) => {
  return axios.post<T>(`${BASE_URL}/${path}`, body, config())
}

export const put = <T>(path: string, body:any, id: number) => {
  return axios.put<T>(`${BASE_URL}/${path}/${id}`, config()).then(res => res)
}

export const deleted = <T>(path: string, id: number) => {
  return axios.delete<T>(`${BASE_URL}/${path}/${id}`, config()).then(res => res)
}