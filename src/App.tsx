import React from 'react'
import Login from './container/login/Login'
import { BrowserRouter as Router, Route } from "react-router-dom";
import './App.css'
import MainContainer from "./container/MainContainer";
import Home from "./container/home/home";
import {PrivateRoute} from "./component/privateRoute/PrivateRoute";
import 'bootstrap/dist/css/bootstrap.min.css';
import Recipe from "./container/recipe/recipe"

const App: React.FC = () => {
    return (
          <Router>
                  <MainContainer>
                      <PrivateRoute exact path="/" component={Home}/>
                      <PrivateRoute exact path="/recette" component={Recipe} />
                  </MainContainer>
                  <Route exact path="/login" component={Login} />
          </Router>
    );
}

export default App;
